# Demo Blog

This is modified version of Yii Blog Demo.

It is a demo, intended to show how to use some of my extensions for yii:

## Extensions included

 * [Gallery Manager](https://bitbucket.org/z_bodya/gallerymanager)
 * [Image Attachment](https://bitbucket.org/z_bodya/yii-image-attachment)
 * [TinyMce](https://bitbucket.org/z_bodya/yii-tinymce) + [ElFinder](https://bitbucket.org/z_bodya/yii-elfinder)


## Install instructions

1. Clone repository
2. Install composer dependencies - inside protected run `php composer.phar install` (http://getcomposer.org/download/)
3. Give web-server write permissions to content folders (in typical case: `chmod -R 777 assets protected/runtime gallery uploads images`)
4. Run it :)

