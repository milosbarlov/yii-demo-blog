<div class="form">

<?php $form=$this->beginWidget('CActiveForm'); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo CHtml::errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>80,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>
    <div class="row bootstrap">
        <label>Cover</label>

            <div>
                <?php
                $this->widget('ImageAttachmentWidget', array(
                    'model' => $model,
                    'behaviorName' => 'coverBehavior',
                    'apiRoute' => 'api/saveImageAttachment',
                ));
                ?>
            </div>

    </div>
	<div class="row">
		<?php echo $form->labelEx($model,'content'); ?>
		<?php
        $this->widget('TinyMce', array(
            'model' => $model,
            'attribute' => "content",
            'compressorRoute' => 'api/tinyMceCompressor',
            'spellcheckerUrl' => 'http://speller.yandex.net/services/tinyspell',
            'fileManager' => array(
                'class' => 'TinyMceElFinder',
                'connectorRoute' => 'api/elFinderConnector',
            ),
        ));
        ?>
		<?php echo $form->error($model,'content'); ?>

    </div>

	<div class="row">
		<?php echo $form->labelEx($model,'tags'); ?>
		<?php $this->widget('CAutoComplete', array(
			'model'=>$model,
			'attribute'=>'tags',
			'url'=>array('suggestTags'),
			'multiple'=>true,
			'htmlOptions'=>array('size'=>50),
		)); ?>
		<p class="hint">Please separate different tags with commas.</p>
		<?php echo $form->error($model,'tags'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',Lookup::items('PostStatus')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

    <div class="row bootstrap">
        <?php echo $form->label($model, 'gallery_id')?>

        <?php
        if ($model->galleryBehavior->getGallery() === null) {
            echo '<p>Before add photos, you need to save at least once</p>';
        } else {
            $this->widget('GalleryManager', array(
                'gallery' => $model->galleryBehavior->getGallery(),
                'controllerRoute'=>'galleryApi'
            ));
        }
        ?>
    </div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->